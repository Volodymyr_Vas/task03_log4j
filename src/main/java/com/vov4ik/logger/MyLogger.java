package com.vov4ik.logger;

import org.apache.logging.log4j.*;

public class MyLogger {
    private static Logger logger = LogManager.getLogger(MyLogger.class);

    public void performSomeTask(){
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");
    }
}
