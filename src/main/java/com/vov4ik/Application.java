package com.vov4ik;

import com.vov4ik.logger.MyLogger;

public class Application {
    public static void main(String[] args) {
        MyLogger myLogger = new MyLogger();
        myLogger.performSomeTask();
    }
}
